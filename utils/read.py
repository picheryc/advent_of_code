import os.path
from os import getcwd


def input_as_lines() -> [str]:
    inp = "{0}/input.txt".format(os.path.dirname(getcwd()))
    f = open(inp, "r")
    return list(map(lambda k: k[:-1], f.readlines()))


def example_as_lines() -> [str]:
    inp = "{0}/example.txt".format(os.path.dirname(getcwd()))
    f = open(inp, "r")
    return list(map(lambda k: k[:-1], f.readlines()))
