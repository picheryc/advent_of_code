from utils.read import input_as_lines, example_as_lines


def part1(grid):
    visible_count = 2 * len(grid) + 2 * len(grid[0]) - 4
    for x in range(1, len(grid[0]) - 1):
        for y in range(1, len(grid) - 1):
            val = grid[y][x]
            visible_left = len([grid[y][x0] for x0 in range(0, x) if grid[y][x0] >= val]) == 0
            visible_right = len([grid[y][x0] for x0 in range(x + 1, len(grid[0])) if grid[y][x0] >= val]) == 0
            visible_up = len([grid[y0][x] for y0 in range(0, y) if grid[y0][x] >= val]) == 0
            visible_down = len([grid[y0][x] for y0 in range(y + 1, len(grid)) if grid[y0][x] >= val]) == 0
            if visible_down or visible_up or visible_right or visible_left:
                visible_count += 1
    return visible_count


if __name__ == "__main__":
    lines = input_as_lines()
    grid = [[int(x) for x in line] for line in lines]
    print(part1(grid))
