from utils.read import input_as_lines, example_as_lines


def part1(grid):
    len_x = len(grid[0])
    len_y = len(grid)
    visible_grid = [[x for x in y] for y in
                    [[1] * len_x] + [[1] + [0] * (len_x - 2) + [1]] * (len_y - 2) + [
                        [1] * len_x]]
    for x in range(1, len_x - 1):
        max_tree_height = grid[0][x]
        for y in range(1, len_y - 1):
            tree_height = grid[y][x]
            if max_tree_height < tree_height:
                visible_grid[y][x] = 1
            max_tree_height = max(tree_height, max_tree_height)
        max_tree_height = grid[len_y - 1][x]
        for y in reversed(range(1, len_x - 1)):
            tree_height = grid[y][x]
            if max_tree_height < tree_height:
                visible_grid[y][x] = 1
            max_tree_height = max(tree_height, max_tree_height)
    for y in range(1, len_y - 1):
        max_tree_height = grid[y][0]
        for x in range(1, len_x - 1):
            tree_height = grid[y][x]
            if max_tree_height < tree_height:
                visible_grid[y][x] = 1
            max_tree_height = max(tree_height, max_tree_height)
        max_tree_height = grid[y][len_x - 1]
        for x in reversed(range(1, len_x - 1)):
            tree_height = grid[y][x]
            if max_tree_height < tree_height:
                visible_grid[y][x] = 1
            max_tree_height = max(tree_height, max_tree_height)
    return sum([sum(x) for x in visible_grid])


if __name__ == "__main__":
    lines = input_as_lines()
    grid = [[int(x) for x in line] for line in lines]
    print(part1(grid))
