from utils.read import input_as_lines, example_as_lines


def part2(grid):
    scenic_scores = []
    for x in range(1, len(grid[0]) - 1):
        for y in range(1, len(grid) - 1):
            val = grid[y][x]
            visible_left = 0
            for x0 in range(x - 1, -1, -1):
                visible_left += 1
                if grid[y][x0] >= val:
                    break
            visible_right = 0
            for x0 in range(x + 1, len(grid[0])):
                visible_right += 1
                if grid[y][x0] >= val:
                    break
            visible_up = 0
            for y0 in range(y - 1, -1, -1):
                visible_up += 1
                if grid[y0][x] >= val:
                    break
            visible_down = 0
            for y0 in range(y + 1, len(grid)):
                visible_down += 1
                if grid[y0][x] >= val:
                    break
            scenic_scores.append(visible_down * visible_up * visible_right * visible_left)
    return max(scenic_scores)


if __name__ == "__main__":
    lines = input_as_lines()
    grid = [[int(x) for x in line] for line in lines]
    print(part2(grid))
