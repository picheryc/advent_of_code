from utils.read import input_as_lines, example_as_lines
import sys

sys.setrecursionlimit(100000)


def find_char(lines, char):
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x] == char:
                return x, y


def get_h(current, lines):
    char = lines[current[1]][current[0]]
    if char == "S":
        char = "a"
    if char == "E":
        char = "z"
    return ord("z") - ord(char)


def get_adjactents(current, max_x, max_y, hs, closed_list):
    x, y = current
    weight = hs[current]
    nexts = []
    if x > 0:
        nexts.append((x - 1, y))
    if x < max_x:
        nexts.append((x + 1, y))
    if y > 0:
        nexts.append((x, y - 1))
    if y < max_y:
        nexts.append((x, y + 1))
    not_in_closed = list(filter(lambda next: next not in closed_list, nexts))
    reachable = list(filter(lambda next: weight - hs[next] <= 1, not_in_closed))
    return reachable


def move(start, end, lines):
    max_x = len(lines[0]) - 1
    max_y = len(lines) - 1
    open_list = []
    closed_list = []
    open_list.append(start)
    parents = {start: None}
    gs = {start: 0}
    hs = {}
    for x in range(max_x + 1):
        for y in range(max_y + 1):
            hs[(x, y)] = get_h((x, y), lines)
    while True:
        fs = list(map(lambda x: hs[x] + gs[x], open_list))
        current = open_list[fs.index(min(fs))]
        if current == end:
            parent = parents[current]
            count = 0
            while parent is not None:
                count += 1
                parent = parents[parent]
            return count
        open_list.remove(current)
        closed_list.append(current)
        adjacents = get_adjactents(current, max_x, max_y, hs, closed_list)
        for adjacent in adjacents:
            if adjacent not in open_list:
                open_list.append(adjacent)
                parents[adjacent] = current
                gs[adjacent] = gs[current] + 1
            else:
                if gs[adjacent] > gs[current] + 1:
                    parents[adjacent] = current
                    gs[adjacent] = gs[current] + 1


if __name__ == "__main__":
    lines = input_as_lines()
    start = find_char(lines, "S")
    end = find_char(lines, "E")
    print(move(start, end, lines))
