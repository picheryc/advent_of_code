from utils.read import input_as_lines, example_as_lines
import sys

sys.setrecursionlimit(100000)


def find_start(lines):
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x] == "S":
                return [x, y]


def move(start, lines, current_char, current_length, lengths, already_visited, dead_ends):
    if len(lengths) > 0 and current_length > min(lengths):
        dead_ends.append(already_visited)
        return
    print(len(dead_ends))
    for dead_end in dead_ends:
        if all(elem in already_visited for elem in dead_end):
            print("found")
            dead_ends.append(already_visited)
            return
    x = start[0]
    y = start[1]
    max_x = len(lines[0]) - 2
    max_y = len(lines) - 2

    if current_char == "E":
        lengths.append(current_length)
    if x <= max_x:
        next_pos = [x + 1, y]
        if next_pos not in already_visited:
            next_char = lines[y][x + 1]
            if next_char != "E" and ord(next_char) - ord(current_char) <= 1 or current_char == "z" and next_char == "E":
                move(next_pos, lines, next_char, current_length + 1, lengths, [x for x in already_visited + [next_pos]],
                     dead_ends)

    if x >= 1:
        next_pos = [x - 1, y]
        if next_pos not in already_visited:
            next_char = lines[y][x - 1]
            if next_char != "E" and ord(next_char) - ord(current_char) <= 1 or current_char == "z" and next_char == "E":
                move(next_pos, lines, next_char, current_length + 1, lengths, [x for x in already_visited + [next_pos]],
                     dead_ends)

    if y >= 1:
        next_pos = [x, y - 1]
        if next_pos not in already_visited:
            next_char = lines[y - 1][x]
            if next_char != "E" and ord(next_char) - ord(current_char) <= 1 or current_char == "z" and next_char == "E":
                move(next_pos, lines, next_char, current_length + 1, lengths, [x for x in already_visited + [next_pos]],
                     dead_ends)

    if y <= max_y:
        next_pos = [x, y + 1]
        if next_pos not in already_visited:
            next_char = lines[y + 1][x]
            if next_char != "E" and ord(next_char) - ord(current_char) <= 1 or current_char == "z" and next_char == "E":
                move(next_pos, lines, next_char, current_length + 1, lengths, [x for x in already_visited + [next_pos]],
                     dead_ends)
    dead_ends.append(already_visited)


if __name__ == "__main__":
    lines = input_as_lines()
    start = find_start(lines)
    lengths = []
    move(start, lines, "a", 0, lengths, [start], [])
    print(min(lengths))
