from utils.read import input_as_lines, example_as_lines
import sys

sys.setrecursionlimit(100000)

def get_h(index, lines_string):
    char = lines_string[index]
    if char == "S":
        char = "a"
    if char == "E":
        char = "z"
    return ord("z") - ord(char)


def get_adjactents(current, len_x, len_y, hs, closed_list):
    x = current % len_x
    y = int(current / len_x)
    weight = hs[current]
    nexts = []
    if x > 0:
        nexts.append(current - 1)
    if x < len_x - 1:
        nexts.append(current + 1)
    if y > 0:
        nexts.append(current - len_x)
    if y < len_y - 1:
        nexts.append(current + len_x)
    not_in_closed = list(filter(lambda next: next not in closed_list, nexts))
    reachable = list(filter(lambda next: weight - hs[next] <= 1, not_in_closed))
    return reachable


def move(start, end, len_x, len_y, hs):
    open_list = []
    closed_list = []
    open_list.append(start)
    parents = {start: None}
    gs = {start: 0}
    fs = {start: hs[start]}
    while True:
        if len(open_list) == 0:
            return
        f_s = list(map(lambda x: fs[x], open_list))
        current = open_list[f_s.index(min(f_s))]
        if current == end:
            parent = parents[current]
            count = 0
            while parent is not None:
                count += 1
                parent = parents[parent]
            return count
        open_list.remove(current)
        closed_list.append(current)
        adjacents = get_adjactents(current, len_x, len_y, hs, closed_list)
        for adjacent in adjacents:
            if adjacent not in open_list:
                open_list.append(adjacent)
                parents[adjacent] = current
                gs[adjacent] = gs[current] + 1
                fs[adjacent] = gs[adjacent] + hs[adjacent]
            else:
                if gs[adjacent] > gs[current] + 1:
                    parents[adjacent] = current
                    gs[adjacent] = gs[current] + 1
                    fs[adjacent] = gs[adjacent] + hs[adjacent]


if __name__ == "__main__":
    lines = input_as_lines()
    end = ""
    lines_string = "".join(lines)
    len_x = len(lines[0])
    len_y = len(lines)
    a_s = []
    hs = {}
    for index in range(len(lines_string)):
        hs[index] = get_h(index, lines_string)
        if lines_string[index] == "a":
            a_s.append(index)
        if lines_string[index] == "E":
            end = index
    vals = []
    mini = 1000000
    for a in a_s:
        print("nodes left: ", len(a_s) - a_s.index(a))
        time = move(a, end, len_x, len_y, hs)
        if time is not None:
            mini = min(time, mini)
    print(mini)
