from utils.read import input_as_lines, example_as_lines


def part_one(sections):
    count = 0
    for section in sections:
        first_range = [x for x in range(int(section[0][0]), int(section[0][1]) + 1)]
        second_range = [y for y in range(int(section[1][0]), int(section[1][1]) + 1)]
        if all(elem in first_range for elem in second_range) or all(elem in second_range for elem in first_range):
            count += 1
    return count


def part_two(sections):
    count = 0
    for section in sections:
        first_range = [x for x in range(int(section[0][0]), int(section[0][1]) + 1)]
        second_range = [y for y in range(int(section[1][0]), int(section[1][1]) + 1)]
        if any(elem in first_range for elem in second_range):
            count += 1
    return count


if __name__ == "__main__":
    lines = input_as_lines()
    sections = [[section[0].split("-"), section[1].split("-")] for section in [line.split(",") for line in lines]]
    print(part_one(sections))
    print(part_two(sections))
