from utils.read import input_as_lines, example_as_lines

if __name__ == "__main__":
    lines = input_as_lines()
    calories = []
    current_sum = 0
    for cal in lines:
        if cal == "":
            calories.append(current_sum)
            current_sum = 0
        else:
            current_sum += int(cal)
    print("result part 1:")
    print(max(calories))
    print("result part 2:")
    print(sum(sorted(calories)[-3:]))
