import time

from utils.read import input_as_lines, example_as_lines


def get_intersections(parsed_lines):
    outer_lines = []
    for lin in parsed_lines:
        (x, y), (bx, by) = lin
        dist = abs(x - bx) + abs(y - by) + 1
        left = (x - dist, y)
        top = (x, y + dist)
        right = (x + dist, y)
        bottom = (x, y - dist)
        outer_lines.append([line(left, top), line(top, right), line(right, bottom), line(bottom, left)])
    intersections = []
    for i in range(len(outer_lines) - 1):
        outer_line_a = outer_lines[i]
        for line_a in outer_line_a:
            for j in range(i - 1, len(outer_lines)):
                outer_line_b = outer_lines[j]
                for line_b in outer_line_b:
                    intersect = intersection(line_a, line_b)
                    if intersect:
                        if not intersect in intersections:
                            intersections.append(intersect)
    return intersections


def line(p1, p2):
    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0] * p2[1] - p2[0] * p1[1])
    return A, B, -C


def intersection(L1, L2):
    D = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = int(Dx / D)
        y = int(Dy / D)
        return x, y
    else:
        return False


if __name__ == "__main__":
    start = time.time()
    lines = input_as_lines()
    parsed_lines = []
    for lin in lines:
        curr = lin[12:]
        x = int(curr.split(", y=")[0])
        y = int(curr.split(", y=")[1].split(": closest beacon is at x=")[0])
        bx = int(curr.split(", y=")[1].split(": closest beacon is at x=")[1].split(", y=")[0])
        by = int(curr.split(", y=")[2])
        parsed_lines.append([(x, y), (bx, by)])
    intersections = get_intersections(parsed_lines)
    detects = [(x[0][0], x[0][1], abs(x[0][0] - x[1][0]) + abs(x[0][1] - x[1][1])) for x in parsed_lines]
    candidates = []
    for intersect in intersections:
        not_in_cluster = []
        x, y = intersect
        for detect in detects:
            x0, y0, dist = detect
            if abs(y - y0) + abs(x - x0) <= dist:
                not_in_cluster.append(False)
                break
        if len(not_in_cluster) == 0:
            candidates.append(intersect)
    answer = [x for x in candidates if 0 < x[0] < 4000000 and 0 < x[1] < 4000000][0]
    print(answer[0] * 4000000 + answer[1])
    end = time.time()
    print(end - start)

