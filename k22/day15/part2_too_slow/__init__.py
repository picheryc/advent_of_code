from utils.read import input_as_lines, example_as_lines

def find_boundaries(parsed_lines):
    x_mins = []
    x_maxs = []
    y_mins = []
    y_maxs = []
    for line in parsed_lines:
        [(x, y), (bx, by)] = line
        x_mins.append(x - abs(x - bx))
        x_maxs.append(x + abs(x - bx))
        y_mins.append(y - abs(y - by))
        y_maxs.append(y + abs(y - by))
    return min(x_mins), max(x_maxs), min(y_mins), max(y_maxs)


def explore(k, detects, center):
    global x, y
    to_explores = set([(x, center[1] + k) for x in range(center[0] - k, center[0] + k + 1)]
                      + [(x, center[1] - k) for x in range(center[0] - k, center[0] + k + 1)]
                      + [(center[0] + k, y) for y in range(center[1] - k, center[1] + k + 1)]
                      + [(center[0] - k, y) for y in range(center[1] - k, center[1] + k + 1)])
    for to_explore in to_explores:
        x0, y0 = to_explore
        detected = False
        for detect in detects:
            (x, y, dist) = detect
            if (abs(y - y0) + abs(x - x0)) <= dist:
                detected = True
                break
        if not detected:
            return to_explore
    return None


if __name__ == "__main__":
    lines = input_as_lines()
    parsed_lines = []
    for line in lines:
        curr = line[12:]
        x = int(curr.split(", y=")[0])
        y = int(curr.split(", y=")[1].split(": closest beacon is at x=")[0])
        bx = int(curr.split(", y=")[1].split(": closest beacon is at x=")[1].split(", y=")[0])
        by = int(curr.split(", y=")[2])
        parsed_lines.append([(x, y), (bx, by)])
    xmin, xmax, ymin, ymax = find_boundaries(parsed_lines)
    detects = [(x[0][0], x[0][1], abs(x[0][0] - x[1][0]) + abs(x[0][1] - x[1][1])) for x in parsed_lines]
    total_weight = sum([x[2] for x in detects])
    xx = int(sum([x[0] * x[2] / total_weight for x in detects]))
    yy = int(sum([x[1] * x[2] / total_weight for x in detects]))
    k = -1
    while True:
        k += 1
        found = explore(k, detects, (xx, yy))
        if found is not None:
            print(found)
            break
