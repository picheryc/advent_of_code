from utils.read import input_as_lines, example_as_lines


def find_not_beacon(parsed_lines, y0):
    set_x = []
    for line in parsed_lines:
        [(x, y), (bx, by)] = line
        dist = abs(y - by) + abs(x - bx)
        dist_to_target = abs(y - y0)
        if dist_to_target <= dist:
            width_x = abs(dist - dist_to_target)
            set_x.extend([k for k in range(x - width_x, x + width_x + 1)])
    beacon_at_y0 = [x[1][0] for x in parsed_lines if x[1][1] == y0]
    sensor_at_y0 = [x[0][0] for x in parsed_lines if x[0][1] == y0]
    set_x = [x for x in set(set_x) if x not in beacon_at_y0 and x not in sensor_at_y0]
    return set_x


if __name__ == "__main__":
    lines = input_as_lines()
    parsed_lines = []
    for line in lines:
        curr = line[12:]
        x = int(curr.split(", y=")[0])
        y = int(curr.split(", y=")[1].split(": closest beacon is at x=")[0])
        bx = int(curr.split(", y=")[1].split(": closest beacon is at x=")[1].split(", y=")[0])
        by = int(curr.split(", y=")[2])
        parsed_lines.append([(x, y), (bx, by)])
    print(len(find_not_beacon(parsed_lines, 2000000)))
