from utils.read import input_as_lines, example_as_lines

points = {"X": 1, "Y": 2, "Z": 3}
op_moves = ["A", "B", "C"]
my_moves = ["X", "Y", "Z"]


def total_points(moves):
    total = 0
    for move in moves:
        my_move = move[1]
        op_move = move[0]
        should_play = "X"
        if my_move == "X":
            should_play = my_moves[(op_moves.index(op_move) - 1) % 3]
        if my_move == "Y":
            should_play = my_moves[op_moves.index(op_move)]
        if my_move == "Z":
            should_play = my_moves[(op_moves.index(op_move) + 1) % 3]
        total += points[should_play]
        op_move = move[0]
        if op_moves.index(op_move) == my_moves.index(should_play):
            total += 3
        if (op_moves.index(op_move) + 1) % 3 == my_moves.index(should_play):
            total += 6
    return total


if __name__ == "__main__":
    lines = input_as_lines()
    moves = [line.split(" ") for line in lines]
    print(total_points(moves))
