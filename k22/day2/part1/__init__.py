from utils.read import input_as_lines, example_as_lines

points = {"X": 1, "Y": 2, "Z": 3}
op_moves = ["A", "B", "C"]
my_moves = ["X", "Y", "Z"]


def total_points(moves):
    total = 0
    for move in moves:
        my_move = move[1]
        total += points[my_move]
        op_move = move[0]
        if op_moves.index(op_move) == my_moves.index(my_move):
            total += 3
        if (op_moves.index(op_move) + 1) % 3 == my_moves.index(my_move):
            total += 6
    return total


if __name__ == "__main__":
    lines = input_as_lines()
    moves = [line.split(" ") for line in lines]
    print(total_points(moves))
