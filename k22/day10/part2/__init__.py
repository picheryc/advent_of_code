from utils.read import input_as_lines, example_as_lines

if __name__ == "__main__":
    k = -1
    X = 1
    lines = [x.split(" ") for x in input_as_lines()]
    screen = []
    current_line = []
    for line in lines:
        if len(line) == 1:
            k += 1
            if k == len(screen) * 40 + 40:
                screen.append(current_line)
                current_line = []
            crt = k % 40
            if X in range(max(0, crt - 1), min(crt + 2, 40)):
                current_line.append("#")
            else:
                current_line.append(".")
        else:
            k += 1
            if k == len(screen) * 40 + 40:
                screen.append(current_line)
                current_line = []
            crt = k % 40
            if X in range(max(0, crt - 1), min(crt + 2, 40)):
                current_line.append("#")
            else:
                current_line.append(".")
            k += 1
            if k == len(screen) * 40 + 40:
                screen.append(current_line)
                current_line = []
            crt = k % 40
            if X in range(max(0, crt - 1), min(crt + 2, 40)):
                current_line.append("#")
            else:
                current_line.append(".")
            X += int(line[1])
    screen.append(current_line)
    for x in screen:
        print(x)
