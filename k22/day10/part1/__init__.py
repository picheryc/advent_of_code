from utils.read import input_as_lines, example_as_lines

if __name__ == "__main__":
    k = 0
    X = 1
    lines = [x.split(" ") for x in input_as_lines()]
    signals = []
    for line in lines:
        if len(line) == 1:
            k += 1
            if k >= len(signals) * 40 + 20:
                signals.append(X * (len(signals) * 40 + 20))
        else:
            k += 2
            if k >= len(signals) * 40 + 20:
                signals.append(X * (len(signals) * 40 + 20))
            X += int(line[1])
    print(sum(signals))
