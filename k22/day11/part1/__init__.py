from utils.read import input_as_lines, example_as_lines


def multiply_by_self(x):
    return x * x


plus = lambda y: lambda x: x + y
multiply_by = lambda y: lambda x: x * y

def get_monkies(groups):
    monkies = []
    for group in groups:
        monkey = []
        items = [int(x) for x in group[1].split("items: ")[1].split(", ")]
        monkey.append(items)
        raw_operation = group[2].split("new = ")[1]

        if " * " in raw_operation:
            if " * old" in raw_operation:
                monkey.append(multiply_by_self)
            else:
                multiplyer = int(raw_operation.split(" * ")[1])
                monkey.append(multiply_by(multiplyer))
        if " + " in raw_operation:
            adder = int(raw_operation.split(" + ")[1])
            monkey.append(plus(adder))
        divider = int(group[3].split(" divisible by ")[1])
        monkey.append(divider)
        true_monkey = int(group[4].split("throw to monkey ")[1])
        false_monkey = int(group[5].split("throw to monkey ")[1])
        monkey.append([true_monkey, false_monkey])
        monkies.append(monkey)
    return monkies


def make_a_round(monkies, inspected_items):
    for monkey_index in range(len(monkies)):
        monkey = monkies[monkey_index]
        inspected_items[monkey_index] += len(monkey[0])
        for item in monkey[0]:
            new_item = int(monkey[1](item) / 3)
            if new_item % monkey[2] == 0:
                monkies[monkey[3][0]][0].append(new_item)
            else:
                monkies[monkey[3][1]][0].append(new_item)
        monkey[0] = []

if __name__ == "__main__":
    lines = input_as_lines()
    groups = []
    current = []
    for line in lines:
        if line == "":
            groups.append(current)
            current = []
        else:
            current.append(line)
    groups.append(current)
    monkies = get_monkies(groups)
    inspected_items = [0 for x in range(len(monkies))]
    for i in range(20):
        make_a_round(monkies, inspected_items)
    print(sorted(inspected_items)[-1] * sorted(inspected_items)[-2])