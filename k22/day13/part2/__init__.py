from utils.read import input_as_lines, example_as_lines
from functools import cmp_to_key

def parse(line):
    response = []
    current = response
    current_buffer = ""
    parents = {id(current): response}
    for char in line[1:]:
        if char == "[":
            if len(current_buffer) > 0 and current_buffer[-1] == ",":
                current_buffer = current_buffer[:-1]
            if len(current_buffer) > 0 and current_buffer[0] == ",":
                current_buffer = current_buffer[1:]
            if current_buffer != "":
                current.extend([int(x) for x in current_buffer.split(",")])
                current_buffer = ""
            new_array = []
            current.append(new_array)
            parents[id(new_array)] = current
            current = new_array
        elif char == "]":
            if len(current_buffer) > 0 and current_buffer[0] == ",":
                current_buffer = current_buffer[1:]
            if len(current_buffer) > 0 and current_buffer[-1] == ",":
                current_buffer = current_buffer[:-1]
            if current_buffer != "":
                current.extend([int(x) for x in current_buffer.split(",")])
                current_buffer = ""
            current = parents[id(current)]
        else:
            current_buffer += char
    return response


def is_correct(first_array, second_array, correct_array):
    if len(correct_array) > 0:
        return
    if len(first_array) == 0 or len(second_array) == 0:
        if len(first_array) == 0:
            correct_array.append(True)
            return
        correct_array.append(False)
        return
    to_compare_first = first_array[0]
    to_compare_second = second_array[0]
    if hasattr(to_compare_first, "__len__"):
        if hasattr(to_compare_second, "__len__"):
            is_correct(to_compare_first, to_compare_second, correct_array)
        else:
            is_correct(to_compare_first, [to_compare_second], correct_array)
    elif hasattr(to_compare_second, "__len__"):
        is_correct([to_compare_first], to_compare_second, correct_array)
    elif to_compare_first > to_compare_second:
        correct_array.append(False)
        return
    elif to_compare_first < to_compare_second:
        correct_array.append(True)
        return
    is_correct(first_array[1:], second_array[1:], correct_array)


if __name__ == "__main__":
    lines = input_as_lines()
    count = 0
    inputs = []
    toto = [[2]]
    tata = [[6]]
    for k in range(0, len(lines) + 1, 3):
        line = lines[k: k + 2]
        first_array = parse(line[0])
        second_array = parse(line[1])
        inputs.append(first_array)
        inputs.append(second_array)
    inputs.append(toto)
    inputs.append(tata)
    def compare(x, y):
        correct_array = []
        is_correct(x, y, correct_array)
        return -1 if correct_array[0] else 1

    cmp = cmp_to_key(compare)
    inputs.sort(key=cmp)
    print((inputs.index(toto) + 1) * (inputs.index(tata)+1))
