from utils.read import input_as_lines, example_as_lines


def grid(lines):
    coords_x = []
    coords_y = []
    for line in lines:
        for coord in line:
            coords_x.append(coord[0])
            coords_y.append(coord[1])
    min_x = min(coords_x)
    max_x = max(coords_x)
    min_y = min(coords_y)
    max_y = max(coords_y)
    grid = [["." for x in range(max_x - min_x + 3)] for y in range(max_y + 2)]
    grid[0][500 - min_x + 1] = "+"
    for line in lines:
        for k in range(len(line) - 1):
            x0, y0 = line[k]
            x1, y1 = line[k + 1]
            if x0 == x1:
                rang = range(y0, y1 + 1) if y1 > y0 else range(y1, y0 + 1)
                for y in rang:
                    grid[y][x0 - min_x + 1] = "#"
            else:
                rang = range(x0, x1 + 1) if x1 > x0 else range(x1, x0 + 1)
                for x in rang:
                    grid[y0][x - min_x + 1] = "#"
    return grid


def animate_grid(grille, start):
    coords = start
    max_x = len(grille[0]) - 1
    max_y = len(grille) - 1
    while True:
        y = coords[1]
        x = coords[0]
        if x == 0 or x == max_x or y == max_y:
            return True
        if grille[y + 1][x] == '.':
            coords = [x, y + 1]
        elif grille[y + 1][x - 1] == '.':
            coords = [x - 1, y + 1]
        elif grille[y + 1][x + 1] == '.':
            coords = [x + 1, y + 1]
        else:
            grille[y][x] = "o"
            return False


if __name__ == "__main__":
    lines = [[[int(z) for z in y.split(",")] for y in x.split(" -> ")] for x in input_as_lines()]
    grille = grid(lines)
    start = [[x for x in range(len(grille[0])) if grille[0][x] == "+"][0], 0]
    k = 0
    while True:
        fell = animate_grid(grille, start)
        if fell:
            break
        k += 1
    print(k)
