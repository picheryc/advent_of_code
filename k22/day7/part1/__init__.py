from utils.read import input_as_lines, example_as_lines


def create_folders(lines):
    folders = ["/", 0, []]
    current_folder = folders
    parents = {id(current_folder): None}
    for line in lines[1:]:
        if "$ cd " in line:
            new_dir = line[5:]
            if new_dir == "..":
                current_folder = parents[id(current_folder)]
            else:
                new_folder = [new_dir, 0, []]
                parents[id(new_folder)] = current_folder
                current_folder[2].append(new_folder)
                current_folder = new_folder
        elif "dir " in line:
            pass
        elif "$ ls" in line:
            pass
        else:
            current_folder[1] += int(line.split(" ")[0])
    return folders


def fill_sizes(parents, sizes, current_folder, current_path):
    size = current_folder[1]
    for parent in parents:
        if sizes.get(parent) is not None:
            sizes[parent] += size
        else:
            sizes[parent] = size
    if len(current_folder[2]) > 0:
        for next_folder in current_folder[2]:
            next_path = current_path + next_folder[0]
            fill_sizes(parents + [next_path], sizes, next_folder, next_path)


def part_1(folders):
    parents = ["/"]
    sizes = {"/": 0}
    current_folder = folders
    fill_sizes(parents, sizes, current_folder, "/")
    return sizes


if __name__ == "__main__":
    lines = input_as_lines()
    folders = create_folders(lines)
    sizes = part_1(folders)
    print(sum([x for x in sizes.values() if x <= 100000]))
    total_size = sizes["/"]
    unused = 70000000 - total_size
    to_free = 30000000 - unused
    print(min([x for x in sizes.values() if x >= to_free]))
