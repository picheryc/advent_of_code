from utils.read import input_as_lines, example_as_lines


def get_score(commons):
    score = 0
    for common in commons:
        if common.lower() == common:
            score += 1 + ord(common) - ord("a")
        else:
            score += 27 + ord(common) - ord("A")
    return score


if __name__ == "__main__":
    lines = input_as_lines()
    max = int(len(lines) / 3)
    stacks = [lines[3*x: 3*x + 3] for x in range(max)]
    commons = [list(filter(lambda x: x in stack[1] and x in stack[2], stack[0]))[0] for stack in stacks]
    print(get_score(commons))
