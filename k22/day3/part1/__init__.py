from utils.read import input_as_lines, example_as_lines


def get_score(commons):
    score = 0
    for common in commons:
        if common.lower() == common:
            score += 1 + ord(common) - ord("a")
        else:
            score += 27 + ord(common) - ord("A")
    return score


if __name__ == "__main__":
    lines = input_as_lines()
    stacks = [[line[: int(len(line) / 2)], line[int(len(line) / 2):]] for line in lines]
    commons = [list(filter(lambda x: x in stack[1], stack[0]))[0] for stack in stacks]
    print(get_score(commons))
    print(commons)
