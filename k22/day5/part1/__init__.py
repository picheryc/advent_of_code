from utils.read import input_as_lines, example_as_lines


def get_moves_and_instructions(lines):
    moves = []
    instructions = []
    still_moves = True
    for line in lines:
        if line == "":
            still_moves = False
        elif still_moves:
            moves.append(line)
        else:
            instructions.append(line)
    return moves, instructions


def parse_moves(moves):
    length = len(moves[0])
    simplified_moves = []
    for move in moves[:-1]:
        simplified_moves.append([move[x] for x in range(1, length, 4)])
    new_length = len(simplified_moves[0])
    width = len(simplified_moves)
    return [[simplified_moves[y][x] for y in range(width) if simplified_moves[y][x] != " "] for x in range(new_length)]


def move(stack, instruction):
    line_to_destack = instruction[1] - 1
    line_to_stack = instruction[2] - 1
    number_to_destack = instruction[0]
    for a in range(number_to_destack):
        stack_before = stack[line_to_destack]
        stack[line_to_destack] = stack_before[1:]
        stack[line_to_stack] = stack_before[:1] + stack[line_to_stack]
    return stack

def move_part_2(stack, instruction):
    line_to_destack = instruction[1] - 1
    line_to_stack = instruction[2] - 1
    number_to_destack = instruction[0]
    stack_before = stack[line_to_destack]
    stack[line_to_destack] = stack_before[number_to_destack:]
    stack[line_to_stack] = stack_before[:number_to_destack] + stack[line_to_stack]
    return stack


if __name__ == "__main__":
    lines = input_as_lines()
    moves, instructions = get_moves_and_instructions(lines)
    parsed_instructions = [[int(x[1]), int(x[3]), int(x[5])] for x in [y.split(" ") for y in instructions]]
    stack = parse_moves(moves)
    for instruction in parsed_instructions:
        stack = move(stack, instruction)
    print("".join([x[0] for x in stack]))

    stack = parse_moves(moves)
    for instruction in parsed_instructions:
        stack = move_part_2(stack, instruction)
    print("".join([x[0] for x in stack]))
