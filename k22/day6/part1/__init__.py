from utils.read import input_as_lines, example_as_lines

if __name__ == "__main__":
    line = input_as_lines()[0]
    for k in range(len(line)):
        elems = [x for x in line[k: k+4]]
        if len(list(set(elems))) == len(elems):
            print(k + 4)
            break
    for k in range(len(line)):
        elems = [x for x in line[k: k+14]]
        if len(list(set(elems))) == len(elems):
            print(k + 14)
            break

