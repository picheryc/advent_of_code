from utils.read import input_as_lines, example_as_lines

mapping = {"R": [1, 0], "L": [-1, 0], "U": [0, 1], "D": [0, -1]}

if __name__ == "__main__":
    instructions = [[x[0], int(x[1])] for x in [line.split(" ") for line in input_as_lines()]]
    max_move = max([y[1] for y in instructions])
    grid = [[1]]
    head = [0, 0]
    tail = [0, 0]
    positions = [[0, 0]]
    for instruction in instructions:
        direction = mapping[instruction[0]]
        times = instruction[1]
        for x in range(times):
            head = [head[0] + direction[0], head[1] + direction[1]]
            delta_x = abs(head[0] - tail[0])
            delta_y = abs(head[1] - tail[1])
            if delta_x > 1 or delta_y > 1:
                if delta_y == 0:
                    if head[0] > tail[0]:
                        tail = [tail[0] + 1, tail[1]]
                    else:
                        tail = [tail[0] - 1, tail[1]]
                elif delta_x == 0:
                    if head[1] > tail[1]:
                        tail = [tail[0], tail[1] + 1]
                    else:
                        tail = [tail[0], tail[1] - 1]
                else:
                    if head[0] > tail[0]:
                        if head[1] > tail[1]:
                            tail = [tail[0] + 1, tail[1] + 1]
                        else:
                            tail = [tail[0] + 1, tail[1] - 1]
                    else:
                        if head[1] > tail[1]:
                            tail = [tail[0] - 1, tail[1] + 1]
                        else:
                            tail = [tail[0] - 1, tail[1] - 1]
            if tail not in positions:
                positions.append(tail)
    print(len(positions))
