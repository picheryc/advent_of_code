import functools

from utils.read import example_as_lines, input_as_lines


def get_grids(lines: [str]):
    j = 0
    grids = []
    while j < len(lines):
        grids.append([list(filter(lambda x: x != "", lines[k].split(" "))) for k in range(j, j + 5)])
        j += 6
    return grids


def get_lines(grid: [str]):
    return [x for x in grid] + [[gri[x] for gri in grid] for x in range(0, 5)]


def has_full_line(grid: [[str]], numberz: [str]):
    filtered_lines = list(filter(lambda line: all(elem in numberz for elem in line), grid))
    return len(filtered_lines) != 0


def find_lines(gridz: [[[str]]], numberz: [str]):
    return list(filter(lambda grid: has_full_line(get_lines(grid), numberz), gridz))


def get_score(grid, numberz, winning_number):
    all_numbers = functools.reduce(lambda x, y: x + y, grid, [])
    number_not_in_grid = list(filter(lambda x: x not in numberz, all_numbers))
    return sum([int(k) for k in number_not_in_grid]) * winning_number


if __name__ == "__main__":
    input_lines = input_as_lines()
    numbers = input_lines[0].split(",")
    grids = get_grids(input_lines[2:])
    print("answer part 1:")
    k = 0
    grids_with_lines = []
    while len(grids_with_lines) == 0:
        grids_with_lines = find_lines(grids, numbers[:k + 1])
        k += 1
    print(get_score(grids_with_lines[0], numbers[:k], int(numbers[k - 1])))
    print("answer part 2:")
    k = 0
    grids_with_lines = []
    while len(grids_with_lines) < len(grids) - 1:
        grids_with_lines = find_lines(grids, numbers[:k + 1])
        k += 1
    loosing_grid = list(filter(lambda grid: grid not in grids_with_lines, grids))[0]
    found = False
    while not found:
        found = len(find_lines([loosing_grid], numbers[:k+1])) != 0
        k += 1
    print(get_score(loosing_grid, numbers[:k], int(numbers[k - 1])))
