import functools

from utils.read import input_as_lines, example_as_lines


def part1(lines: [str]) -> [int, int]:
    ret = [0, 0]
    for line in lines:
        [instruction, number] = line.split(" ")
        if instruction == "forward":
            ret = [ret[0] + int(number), ret[1]]
        elif instruction == "up":
            ret = [ret[0], ret[1] - int(number)]
        else:
            ret = [ret[0], ret[1] + int(number)]
    return ret

def part2(lines: [str]) -> int:
    aim = 0
    x = 0
    y = 0
    for line in lines:
        [instruction, number] = line.split(" ")
        X = int(number)
        if instruction == "forward":
            x += X
            y += aim * X
        elif instruction == "up":
            aim -= X
        else:
            aim += X
    return [x, y]

if __name__ == "__main__":
    lines = input_as_lines()
    instructions = part1(lines)
    print(instructions)
    print(instructions[0] * instructions[1])
    [x2, y2] = part2(lines)
    print(x2 * y2)
