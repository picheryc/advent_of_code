import functools

from utils.read import example_as_lines, input_as_lines


def plus_one(values):
    return [[y + 1 for y in x] for x in values]


def find_booming(values, already_boomed):
    booming = []
    for y in range(len(values)):
        for x in range(len(values[0])):
            if [x, y] not in already_boomed:
                if values[y][x] > 9:
                    booming.append([x, y])
    return booming


def get_to_increase(x, y, xmax, ymax, already_boomed):
    positions = []
    if x > 0:
        positions.append([x - 1, y])
        if y > 0:
            positions.append([x - 1, y - 1])
        if y < ymax:
            positions.append([x - 1, y + 1])
    if x < xmax:
        positions.append([x + 1, y])
        if y > 0:
            positions.append([x + 1, y - 1])
        if y < ymax:
            positions.append([x + 1, y + 1])
    if y > 0:
        positions.append([x, y - 1])
    if y < ymax:
        positions.append([x, y + 1])
    return list(filter(lambda z: z not in already_boomed, positions))


def boom(state, will_boom, already_boomed):
    for [x, y] in will_boom:
        to_increase = get_to_increase(x, y, len(state[0]) - 1, len(state) - 1, already_boomed)
        for [x_to_increase, y_to_increase] in to_increase:
            state[y_to_increase][x_to_increase] += 1
    return state


def one_step(values):
    state = plus_one(values)
    already_boomed = []
    will_boom = find_booming(state, already_boomed)
    while len(will_boom) > 0:
        already_boomed += will_boom
        state = boom(state, will_boom, already_boomed)
        will_boom = find_booming(state, already_boomed)
    return [[0 if val > 9 else val for val in line] for line in state], len(already_boomed)


if __name__ == "__main__":
    lines = input_as_lines()
    values = [[int(x) for x in y] for y in lines]
    k = 0
    boom_count = 0
    while k < 100:
        values, booms = one_step(values)
        boom_count += booms
        k += 1
    print(boom_count)
