from utils.read import example_as_lines, input_as_lines


def get_rates_part_1(lines: [str]) -> [str]:
    line_length = len(lines[0])
    lines_length = len(lines)
    columns = [sum([int(lines[j][i]) for j in range(lines_length)]) for i in range(line_length)]
    gamma_rate = [0 if k <= lines_length / 2 else 1 for k in columns]
    epsilon_rate = [0 if k == 1 else 1 for k in gamma_rate]
    return [gamma_rate, epsilon_rate]


def get_rates_part_2(lines: [str]) -> [str]:
    lines_for_oxygen = [k for k in lines]
    lines_for_co2 = [k for k in lines]
    k = 0
    while len(lines_for_oxygen) > 1:
        majority = "0" if sum([int(line[k]) for line in lines_for_oxygen]) < len(lines_for_oxygen) / 2 else "1"
        lines_for_oxygen = list(filter(lambda x: x[k] == majority, lines_for_oxygen))
        k += 1
    k = 0
    while len(lines_for_co2) > 1:
        minority = "0" if sum([int(line[k]) for line in lines_for_co2]) >= len(lines_for_co2) / 2 else "1"
        lines_for_co2 = list(filter(lambda x: x[k] == minority, lines_for_co2))
        k += 1
    return [lines_for_oxygen[0], lines_for_co2[0]]


def get_decimal(binary: [int]) -> int:
    return int("".join([str(k) for k in binary]), 2)


if __name__ == "__main__":
    input_lines = input_as_lines()
    [gamma_rate_bin, epsilon_rate_bin] = get_rates_part_1(input_lines)
    print("result part 1:")
    print(get_decimal(gamma_rate_bin) * get_decimal(epsilon_rate_bin))
    [oxygen_bin, co2_bin] = get_rates_part_2(input_lines)
    print("result part 2:")
    print(get_decimal(oxygen_bin) * get_decimal(co2_bin))
