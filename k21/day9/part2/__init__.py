import functools

import numpy

from utils.read import example_as_lines, input_as_lines


def explore_bassin(map, current_point, bassin):
    x = current_point[0]
    y = current_point[1]
    if map[y][x] == "9":
        return
    bassin.append(current_point)
    if not [x + 1, y] in bassin:
        explore_bassin(map, [x + 1, y], bassin)
    if not [x - 1, y] in bassin:
        explore_bassin(map, [x - 1, y], bassin)
    if not [x, y + 1] in bassin:
        explore_bassin(map, [x, y + 1], bassin)
    if not [x, y - 1] in bassin:
        explore_bassin(map, [x, y - 1], bassin)
    return bassin


def find_basins(map):
    low_points = find_low_points(map)
    basins = []
    for low_point in low_points:
        basins.append(explore_bassin(map, low_point, []))
    return basins


def find_low_points(map):
    low_points = []
    for x in range(1, len(map[0]) - 1):
        for y in range(1, len(map) - 1):
            value = int(map[y][x])
            adjacent_values = [int(map[y + 1][x]), int(map[y - 1][x]), int(map[y][x + 1]), int(map[y][x - 1])]
            if min(adjacent_values) > value:
                low_points.append([x, y])
    return low_points


if __name__ == "__main__":
    map = input_as_lines()
    map_width = len(map[0]) + 2
    modified_map = ["9" * map_width] + ["9" + line + "9" for line in map] + ["9" * map_width]
    print('result part 1:')
    three_largest = sorted([len(x) for x in find_basins(modified_map)])[-3:]
    print(numpy.prod(three_largest))
