import functools

from utils.read import example_as_lines, input_as_lines


def find_low_points(map):
    low_points = []
    for x in range(1, len(map[0]) - 1):
        for y in range(1, len(map) - 1):
            value = int(map[y][x])
            adjacent_values = [int(map[y + 1][x]), int(map[y - 1][x]), int(map[y][x + 1]), int(map[y][x - 1])]
            if min(adjacent_values) > value:
                low_points.append(value + 1)
    return low_points


if __name__ == "__main__":
    map = input_as_lines()
    map_width = len(map[0]) + 2
    modified_map = ["9" * map_width] + ["9" + line + "9" for line in map] + ["9" * map_width]
    print('result part 1:')
    print(sum(find_low_points(modified_map)))
