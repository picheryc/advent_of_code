from utils.read import input_as_lines, example_as_lines


def sum_increases(lines: [int]) -> int:
    return sum([1 if lines[k] > lines[k - 1] else 0 for k in range(1, len(lines))])


def create_windows(lines: [int]) -> [int]:
    return [lines[x] + lines[x - 1] + lines[x - 2] for x in range(2, len(lines))]


if __name__ == "__main__":
    str_lines = input_as_lines()
    int_lines = list(map(lambda x: int(x), str_lines))
    print(sum_increases(int_lines))
    dows = create_windows(int_lines)
    print(dows)
    print(sum_increases(dows))
