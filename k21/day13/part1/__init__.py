import functools

from utils.read import example_as_lines, input_as_lines

if __name__ == "__main__":
    lines = example_as_lines()
    cut_index = lines.index("")
    points = [[int(x) for x in line.split(",")] for line in lines[:cut_index]]
    instructions = [[line.split("=")[0][-1], int(line.split("=")[1])] for line in lines[cut_index + 1:]]

