import functools

from utils.read import example_as_lines, input_as_lines


def reproduce(number_of_fishes: [int]):
    number_of_0 = number_of_fishes[0]
    return number_of_fishes[1:7] + [number_of_fishes[7] + number_of_0] + [number_of_fishes[8]] + [number_of_0]


if __name__ == "__main__":
    fishes = list(map(lambda x: int(x), input_as_lines()[0].split(",")))
    k = 0
    number_of_fishes = [fishes.count(x) for x in range(0, 9)]
    while k < 256:
        number_of_fishes = reproduce(number_of_fishes)
        k += 1
    print(sum(number_of_fishes))
