import unittest
import math

from k21.day0.part1 import plus


class TestCircle(unittest.TestCase):
    def test_1_plus_1_is_2(self):
        self.assertEqual(2, plus(1, 1))
