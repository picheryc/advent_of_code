import functools

from utils.read import example_as_lines, input_as_lines


def reproduce(fishes: [int]):
    res = []
    for fish in fishes:
        if fish > 0:
            res.append(fish - 1)
        else:
            res.append(6)
            res.append(8)
    return res


if __name__ == "__main__":
    fishes = list(map(lambda x: int(x), input_as_lines()[0].split(",")))
    k = 0
    while k < 256:
        fishes = reproduce(fishes)
        k += 1
        print(k)
    print(len(fishes))
