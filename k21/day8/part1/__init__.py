import functools

from utils.read import example_as_lines, input_as_lines


def get_mapping(coding):
    def get_number(length):
        return list(filter(lambda x: len(x) == length, coding))[0]

    one = get_number(2)
    seven = get_number(3)
    four = get_number(4)
    eight = get_number(7)
    all_letters = ''.join(coding)
    frequencies = [[all_letters.count(letter), letter] for letter in ["a", "b", "c", "d", "e", "f", "g"]]

    def get_letters(frequency):
        return list(filter(lambda x: x[0] == frequency, frequencies))

    b = get_letters(6)[0][1]
    e = get_letters(4)[0][1]
    f = get_letters(9)[0][1]

    a_or_c = get_letters(8)
    c = list(filter(lambda x: x[1] in four, a_or_c))[0][1]
    a = list(filter(lambda x: x[1] not in four, a_or_c))[0][1]

    d_or_g = get_letters(7)
    d = list(filter(lambda x: x[1] in four, d_or_g))[0][1]
    g = list(filter(lambda x: x[1] not in four, d_or_g))[0][1]

    return {a: "a", b: "b", c: "c", d: "d", e: "e", f: "f", g: "g"}


digits = [["a", "b", "c", "e", "f", "g"], ["c", "f"], ["a", "c", "d", "e", "g"], ["a", "c", "d", "f", "g"],
          ["b", "c", "d", "f"], ["a", "b", "d", "f", "g"], ["a", "b", "d", "e", "f", "g"], ["a", "c", "f"],
          ["a", "b", "c", "d", "e", "f", "g"], ["a", "b", "c", "d", "f", "g"]]


def resolve(number, mapping):
    converted = [mapping[letter] for letter in number]
    dig = list(filter(
        lambda digit: len(digit) == len(converted) and all(elem in converted for elem in digit), digits))[0]
    return digits.index(dig)


def part_2(entriez):
    result = 0
    for entry in entriez:
        mapping = get_mapping(entry[0])
        numbers = entry[1]
        result += int("".join([str(resolve(number, mapping)) for number in numbers]))
    return result


if __name__ == "__main__":
    entries = [[half.split(" ") for half in line.split(" | ")] for line in input_as_lines()]
    print("result part1:")
    outputs = functools.reduce(lambda acc, val: acc + val[1], entries, [])
    print(sum([1 if len(output) != 5 and len(output) != 6 else 0 for output in outputs]))
    print("result shorter:")
    print(part_2(entries))
