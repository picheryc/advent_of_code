import functools

from utils.read import example_as_lines, input_as_lines


def get_points_h_v(lines: [str]) -> dict[str, int]:
    instructions = [[inst.split(",") for inst in line.split(" -> ")] for line in lines]
    h_v_instructions = list(
        filter(lambda instruction: instruction[0][0] == instruction[1][0] or instruction[0][1] == instruction[1][1],
               instructions))
    res = {}
    for instruction in h_v_instructions:
        direction = "y" if instruction[0][0] == instruction[1][0] else "x"
        if direction == "x":
            x1 = int(instruction[0][0])
            x2 = int(instruction[1][0])
            [minimum, maximum] = [x1, x2] if x1 < x2 else [x2, x1]
            points = [[str(x), instruction[0][1]] for x in range(minimum, maximum + 1)]
        else:
            y1 = int(instruction[0][1])
            y2 = int(instruction[1][1])
            [minimum, maximum] = [y1, y2] if y1 < y2 else [y2, y1]
            points = [[instruction[0][0], str(y)] for y in range(minimum, maximum + 1)]
        join_points(points, res)
    return res


def join_points(points, res):
    for point in points:
        str_point = ",".join(point)
        if res.get(str_point) is not None:
            res[str_point] += 1
        else:
            res[str_point] = 1


if __name__ == "__main__":
    input_lines = input_as_lines()
    points_dict = get_points_h_v(input_lines)
    print(sum([1 if x > 1 else 0 for x in points_dict.values()]))
