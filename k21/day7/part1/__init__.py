import functools

from utils.read import example_as_lines, input_as_lines


def find_best_h_pos_non_linear(h_poses: [int]):
    mini = min(h_poses)
    maxi = max(h_poses)
    best_h = mini
    best_distances = 0.5 * len(h_poses) * (len(h_poses) + 1) * maxi
    for pos in range(mini, maxi + 1):
        distances = sum([0.5 * abs(x - pos) * (abs(x - pos) + 1) for x in h_poses])
        if best_distances > distances:
            best_h = pos
            best_distances = distances
    return [best_h, best_distances]


def find_best_h_pos(h_poses: [int]):
    mini = min(h_poses)
    maxi = max(h_poses)
    best_h = mini
    best_distances = len(h_poses) * maxi
    for pos in range(mini, maxi + 1):
        distances = sum([abs(x - pos) for x in h_poses])
        if best_distances > distances:
            best_h = pos
            best_distances = distances
    return [best_h, best_distances]


if __name__ == "__main__":
    h_positions = list(map(lambda x: int(x), input_as_lines()[0].split(",")))
    print("result part1:")
    print(find_best_h_pos(h_positions))
    print("result shorter:")
    print(find_best_h_pos_non_linear(h_positions))
