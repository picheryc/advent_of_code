import functools

from utils.read import example_as_lines, input_as_lines


def find_connections(raw_connections, points):
    result = {}
    for point in points:
        list_connections = []
        for raw_connection in raw_connections:
            if raw_connection[0] == point:
                list_connections.append(raw_connection[1])
            if raw_connection[1] == point:
                list_connections.append(raw_connection[0])
        result[point] = list_connections
    return result


def fill_paths(current_point, connections, paths, ongoing_path, visited):
    new_ongoing_path = ongoing_path + [current_point]
    if current_point == "end":
        paths.append(ongoing_path)
        return
    to_visit = list(filter(lambda visit: visit not in visited, connections[current_point]))
    for visit in to_visit:
        new_visited = visited + [visit] if visit == visit.lower() else visited
        fill_paths(visit, connections, paths, new_ongoing_path, new_visited)

if __name__ == "__main__":
    lines = input_as_lines()
    raw_connections = [line.split("-") for line in lines]
    points = list(set(functools.reduce(lambda acc, val: acc + val, raw_connections, [])))
    connections = find_connections(raw_connections, points)
    paths = []
    fill_paths("start", connections, paths, [], ["start"])
    print(len(paths))
