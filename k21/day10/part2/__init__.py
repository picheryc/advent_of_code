import functools

from utils.read import example_as_lines, input_as_lines

opening_chars = ["(", "{", "[", "<"]
closing_chars = [")", "}", "]", ">"]


def find_completions(lines):
    completions = []
    for line in lines:
        current_buffer = []
        valid = True
        for char in line:
            if char in opening_chars:
                current_buffer.append(char)
            else:
                if opening_chars.index(current_buffer[-1]) == closing_chars.index(char):
                    current_buffer = current_buffer[:-1]
                else:
                    valid = False
                    break
        if valid:
            completions.append([closing_chars[opening_chars.index(x)] for x in current_buffer[::-1]])
    return completions


points = {")": 1, "]": 2, "}": 3, ">": 4}


def get_values(completions):
    values = []
    for completion in completions:
        value = 0
        for char in completion:
            value *= 5
            value += points[char]
        values.append(value)
    return values

def find_middle_score(values):
    return sorted(values)[int((len(values)) / 2)]



if __name__ == "__main__":
    lines = input_as_lines()
    completions = find_completions(lines)
    values = get_values(completions)
    print(find_middle_score(values))
