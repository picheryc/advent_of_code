import functools

from utils.read import example_as_lines, input_as_lines

opening_chars = ["(", "{", "[", "<"]
closing_chars = [")", "}", "]", ">"]


def find_invalid_chars(lines):
    invalid_chars = []
    for line in lines:
        current_buffer = []
        for char in line:
            if char in opening_chars:
                current_buffer.append(char)
            else:
                if opening_chars.index(current_buffer[-1]) == closing_chars.index(char):
                    current_buffer = current_buffer[:-1]
                else:
                    invalid_chars.append(char)
                    break
    return invalid_chars


points = {")": 3, "]": 57, "}": 1197, ">": 25137}


def get_values(invalid_chars):
    return sum([points[k] for k in invalid_chars])


if __name__ == "__main__":
    lines = input_as_lines()
    invalid_chars = find_invalid_chars(lines)
    print(get_values(invalid_chars))
